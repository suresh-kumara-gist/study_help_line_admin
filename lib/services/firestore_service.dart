import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:study_help_line_admin/models/register_tutor.dart';
// import 'package:compound/models/user.dart';
import 'package:flutter/services.dart';

class FirestoreService {
  // final CollectionReference _usersCollectionReference =
  //     Firestore.instance.collection('users');
  final CollectionReference _registraionsCollectionReference =
      Firestore.instance.collection('registrations');

  // final StreamController<List<Post>> _postsController =
  //     StreamController<List<Post>>.broadcast();

  Future registerTutor(RegisterTutor registerTutor) async {
    try {
      await _registraionsCollectionReference.add(registerTutor.toMap());
    } catch (e) {
      // TODO: Find or create a way to repeat error handling without so much repeated code
      if (e is PlatformException) {
        return e.message;
      }

      return e.toString();
    }
  }

  Future getRegisteredTutors(
      String selectedClass, List selectedSubjects) async {
    try {
      var documentSnapshots = await _registraionsCollectionReference
          .where('selectedClass', isEqualTo: selectedClass)
          // .where('selectedSubjects', arrayContainsAny: selectedSubjects)
          .getDocuments();

      if (documentSnapshots.documents.isNotEmpty) {
        // print(documentSnapshots.documents);
        return documentSnapshots.documents
            .map((snapshot) => RegisterTutor.getTokenFromMap(
                snapshot.data, snapshot.documentID))
            // .where((mappedItem) => mappedItem.title != null)
            .toList();
      } else {
        return [];
      }
    } catch (e) {
      if (e is PlatformException) {
        return e.message;
      }
      return e.toString();
    }
  }
}
