import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:study_help_line_admin/locator.dart';
import 'package:study_help_line_admin/services/dialog_service.dart';
import 'package:study_help_line_admin/ui/shared/ui_helpers.dart';
import 'package:study_help_line_admin/ui/widgets/busy_button.dart';
import 'package:study_help_line_admin/ui/widgets/input_field.dart';
import 'package:study_help_line_admin/viewmodels/pushnotification_form_view_model.dart';
import 'package:flutter/material.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:stacked/stacked.dart';
// import 'package:flutter_open_whatsapp/flutter_open_whatsapp.dart';

class PushNotificationFormView extends StatefulWidget {
  _PushNotificationFormViewState createState() =>
      _PushNotificationFormViewState();
}

class _PushNotificationFormViewState extends State<PushNotificationFormView> {
  int totalPage = 2;
  int currentPage = 1;

  List<Widget> pageList = List();

  final _formKey = GlobalKey(debugLabel: "push notification form key");
  // final nameController = TextEditingController();
  // final contactNumberController = TextEditingController();
  // final emailController = TextEditingController();
  // final addressController = TextEditingController();

  bool _isChecked = true;
  // Do you have widget
  String selectedClass = "";
  List selectedSubjects;

  List<String> _subjects = [
    "English",
    "Mathematics",
    "Physics",
    "Chemistry",
    "Biology",
    "Computer Science",
    "Accounts",
    "Business Studies",
    "Economics",
    "Infomation Practices",
    "Geography",
    "History",
    "Hindi",
    "Psychology",
    "Sanskrit",
    "Physical Education",
    "Art & Creativity",
  ];

  final DialogService _dialogService = locator<DialogService>();
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  // final referencePhoneController = TextEditingController();
  // final format = DateFormat("dd-MM-yyyy");
  // DateTime dob;
  // int rating;
  // final bANController = TextEditingController();

  // @override
  // void initState() {
  //   super.initState();
  //   selectedRadio = 0;
  // }

  // Changes the selected value on 'onChanged' click on each radio button
  _handleRadioValueChange1(String val) {
    setState(() {
      selectedClass = val;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    totalPage = totalPage;
    getMessage();
  }

  void getMessage() {
    _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
      print('on message $message');
      // setState(() => _message = message["notification"]["title"]);
      // showDialog(
      //   context: context,
      //   builder: (context) => AlertDialog(
      //     content: ListTile(
      //       title: Text(message['notification']['title']),
      //       subtitle: Text(message['notification']['body']),
      //     ),
      //     actions: <Widget>[
      //       FlatButton(
      //         child: Text('Ok'),
      //         onPressed: () => Navigator.of(context).pop(),
      //       ),
      //     ],
      //   ),
      // );
    }, onResume: (Map<String, dynamic> message) async {
      print('on resume $message');
      // setState(() => _message = message["notification"]["title"]);
    }, onLaunch: (Map<String, dynamic> message) async {
      print('on resume $message');
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          content: ListTile(
            title: Text(message['data']['message']),
            // subtitle: Text(message['body']),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('No'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            Spacer(),
            FlatButton(
              child: Text('yes'),
              onPressed: () {
                // FlutterOpenWhatsapp.sendSingleMessage(
                //     "9632324012", "$message['data']['message'] - yes");
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );
      // setState(() => _message = message["notification"]["title"]);
    });
  }

  // Widget getNextButtonWrapper(Widget child) {
  //   if (nextButtonStyle != null) {
  //     return child;
  //   } else {
  //     return Text("Next");
  //   }
  // }

  // Widget getPreviousButtonWrapper(Widget child) {
  //   if (previousButtonStyle != null) {
  //     return child;
  //   } else {
  //     return Text("Previous");
  //   }
  // }

  // Widget getSubmitButtonWrapper(Widget child) {
  //   if (widget.previousButtonStyle != null) {
  //     return child;
  //   } else {
  //     return Text("Submit");
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    pageList = <Widget>[page1(), page2()];

    return ViewModelBuilder<PushNotificationFormViewModel>.reactive(
        viewModelBuilder: () => PushNotificationFormViewModel(),
        builder: (context, model, child) => Scaffold(
            body: SafeArea(
                child: new Form(
                    key: this._formKey,
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            child: pageHolder(pageList),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 20.0),
                            child: Container(
                              height: 50.0,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  currentPage == 1
                                      ? Container()
                                      : FlatButton(
                                          child: Text('Previous'),
                                          // busy: model.busy,
                                          onPressed: () {
                                            setState(() {
                                              currentPage = currentPage - 1;
                                            });
                                          },
                                        ),
                                  currentPage == totalPage
                                      ?
                                      // Row(
                                      //     mainAxisSize: MainAxisSize.max,
                                      //     mainAxisAlignment:
                                      //         MainAxisAlignment.center,
                                      //     children: [
                                      BusyButton(
                                          title: 'Send Notification',
                                          busy: model.busy,
                                          onPressed: () {
                                            model.sendNotification(
                                                // contactNumber:
                                                //     contactNumberController
                                                //         .text,
                                                // name: nameController.text,
                                                // address: addressController.text,
                                                selectedClass: selectedClass,
                                                selectedSubjects:
                                                    selectedSubjects);
                                          },
                                        )
                                      //   ],
                                      // )
                                      : BusyButton(
                                          title: 'Next',
                                          busy: model.busy,
                                          onPressed: () async {
                                            if (currentPage == 1 &&
                                                selectedClass == "") {
                                              await _dialogService.showDialog(
                                                title: 'Error',
                                                description:
                                                    "Please Select Class",
                                              );
                                            } else if (currentPage == 2 &&
                                                selectedSubjects.isEmpty) {
                                              await _dialogService.showDialog(
                                                title: 'Error',
                                                description:
                                                    "Please Select Subjects",
                                              );
                                            } else {
                                              setState(() {
                                                currentPage = currentPage + 1;
                                              });
                                            }
                                          },
                                        ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    )))));
  }

  Widget pageHolder(pageList) {
    for (int i = 1; i <= totalPage; i++) {
      if (currentPage == i) {
        return pageList[i - 1];
      }
    }
    return Container();
  }

  // Widget page1() {
  //   return Container(
  //     child: ListView(
  //       shrinkWrap: true,
  //       children: [
  //         Text(
  //           'Tutor Registration Form',
  //           style: TextStyle(
  //             fontSize: 25,
  //           ),
  //         ),
  //         verticalSpaceLarge,
  //         InputField(
  //           placeholder: 'Name',
  //           controller: nameController,
  //           additionalNote: 'Full Name',
  //           validationMessage: "Name is required.",
  //         ),
  //         verticalSpaceSmall,
  //         InputField(
  //           placeholder: '+91',
  //           controller: contactNumberController,
  //           validationMessage: "Contact number is required.",
  //           additionalNote: 'Contact No.',
  //           textInputType: TextInputType.phone,
  //         ),
  //         verticalSpaceSmall,
  //         InputField(
  //           placeholder: 'Address',
  //           controller: addressController,
  //           textInputType: TextInputType.multiline,
  //         ),
  //       ],
  //     ),
  //   );
  // }

  Widget page1() {
    return Container(
      child: ListView(
        shrinkWrap: true,
        children: [
          Text(
            'Which Class / Course you are Looking For ?',
            style: TextStyle(
              fontSize: 20,
            ),
          ),
          verticalSpaceLarge,
          new Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              ListTile(
                title: Text("Nursery"),
                trailing: new Radio(
                  value: "nursery",
                  groupValue: selectedClass,
                  onChanged: _handleRadioValueChange1,
                ),
              ),
              ListTile(
                title: Text("Class I-V Tuition"),
                trailing: new Radio(
                  value: "class-i-v-tuition",
                  groupValue: selectedClass,
                  onChanged: _handleRadioValueChange1,
                ),
              ),
              ListTile(
                title: Text("Class VI-VII Tuition"),
                trailing: new Radio(
                  value: "class-vi-vii-tuition",
                  groupValue: selectedClass,
                  onChanged: _handleRadioValueChange1,
                ),
              ),
              ListTile(
                title: Text("Class IX-X Tuition"),
                trailing: new Radio(
                  value: "class-ix-x-tuition",
                  groupValue: selectedClass,
                  onChanged: _handleRadioValueChange1,
                ),
              ),
              ListTile(
                title: Text("Class XI-XII Tuition"),
                trailing: new Radio(
                  value: "class-xi-xii-tuition",
                  groupValue: selectedClass,
                  onChanged: _handleRadioValueChange1,
                ),
              ),
              ListTile(
                title: Text("Others"),
                trailing: new Radio(
                  value: "others",
                  groupValue: selectedClass,
                  onChanged: _handleRadioValueChange1,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget page2() {
    return Container(
      child: ListView(
        children: [
          Text("Subject(s) you want to teach ?"),
          CheckboxGroup(
              labels: _subjects,
              onSelected: (List<String> checked) {
                setState(() {
                  selectedSubjects = checked;
                });
              }),
        ],
      ),
    );
  }

  // Future<void> _handleNotification (Map<dynamic, dynamic> message, bool dialog) async {
  //     var data = message['data'] ?? message;
  //     String expectedAttribute = data['expectedAttribute'];
  //     /// [...]
  // }
}
