import 'package:study_help_line_admin/services/authentication_service.dart';
import 'package:study_help_line_admin/services/cloud_storage_service.dart';
import 'package:study_help_line_admin/services/firestore_service.dart';
import 'package:study_help_line_admin/services/push_notification_service.dart';
import 'package:study_help_line_admin/utils/image_selector.dart';
import 'package:get_it/get_it.dart';
import 'package:study_help_line_admin/services/navigation_service.dart';
import 'package:study_help_line_admin/services/dialog_service.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton(() => DialogService());
  // locator.registerLazySingleton(() => AuthenticationService());
  locator.registerLazySingleton(() => FirestoreService());
  locator.registerLazySingleton(() => CloudStorageService());
  // locator.registerLazySingleton(() => ImageSelector());
  // locator.registerLazySingleton(() => PushNotificationService());
}
