import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';

class RegisterTutor {
  final String contactNumber;
  final String name;
  final String address;
  final String selectedClass;
  final List selectedSubjects;
  final String token;
  final FieldValue createdAt; // optional
  final String platform; // optional

  RegisterTutor(
      {this.contactNumber,
      this.name,
      this.address,
      this.selectedClass,
      this.selectedSubjects,
      this.token,
      this.createdAt,
      this.platform});

  Map<String, dynamic> toMap() {
    return {
      'contactNumber': contactNumber,
      'name': name,
      'address': address,
      'selectedClass': selectedClass,
      'selectedSubjects': selectedSubjects,
      'token': token,
      'createdAt': createdAt,
      'platform': platform
    };
  }

  static RegisterTutor fromMap(
      Map<String, dynamic> map, final String documentId) {
    if (map == null) return null;

    return RegisterTutor(
        contactNumber: map['contactNumber'],
        name: map['name'],
        address: map['address'],
        selectedClass: map['selectedClass'],
        selectedSubjects: map['selectedSubjects'],
        token: map['token'],
        platform: map['platform']);
  }

  static String getTokenFromMap(
      Map<String, dynamic> map, final String documentId) {
    if (map == null) return null;

    return map['token'];
  }
}
