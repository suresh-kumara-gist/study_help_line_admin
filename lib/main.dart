import 'package:study_help_line_admin/ui/views/pushnotification_form_view.dart';
// import 'package:compound/ui/views/startup_view.dart';
import 'package:flutter/material.dart';
import 'package:study_help_line_admin/services/navigation_service.dart';
import 'package:study_help_line_admin/services/dialog_service.dart';
import 'managers/dialog_manager.dart';
import 'ui/router.dart';
import 'locator.dart';

void main() {
  // Register all the models and services before the app starts
  setupLocator();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'study help line',
      builder: (context, child) => Navigator(
        key: locator<DialogService>().dialogNavigationKey,
        onGenerateRoute: (settings) => MaterialPageRoute(
            builder: (context) => DialogManager(child: child)),
      ),
      navigatorKey: locator<NavigationService>().navigationKey,
      theme: ThemeData(
        primaryColor: Color(0xff19c7c1),
        textTheme: Theme.of(context).textTheme.apply(
              fontFamily: 'Open Sans',
            ),
      ),
      home: PushNotificationFormView(),
      onGenerateRoute: generateRoute,
    );
  }
}

// import 'dart:convert';

// import 'package:flutter/material.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
// import 'package:http/http.dart' as http;

// void main() => runApp(new MyApp());

// class MyApp extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         // This is the theme of your application.
//         //
//         // Try running your application with "flutter run". You'll see the
//         // application has a blue toolbar. Then, without quitting the app, try
//         // changing the primarySwatch below to Colors.green and then invoke
//         // "hot reload" (press "r" in the console where you ran "flutter run",
//         // or simply save your changes to "hot reload" in a Flutter IDE).
//         // Notice that the counter didn't reset back to zero; the application
//         // is not restarted.
//         primarySwatch: Colors.blue,
//       ),
//       home: MyHomePage(),
//     );
//   }
// }

// class MyHomePage extends StatefulWidget {
//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {
// // Replace with server token from firebase console settings.
//   final String serverToken =
//       'AAAA2yhmBG0:APA91bGg_4nqB7DhqZEOwAls1z8qw4c10YYFPqHLZxegrMv5Y3kOcZJTVfqnv6Pfpsy6gu142P0ZAEC8F-ESXe5FDMGWSvjwcS_wu2Xlv19BPWBSnPdYHLTB0vfFDBaWj0yniIeh9cFO';
//   final FirebaseMessaging firebaseMessaging = FirebaseMessaging();
//   final registrationTokens = [
//     'eqBBkbFD5zc:APA91bEWf0tHyRkP8Awm9ujMbyHbgMYdKUJnARC9C7etMn25abk8HXfq94Qwa918dYGjLiNUmNXH2nO8B4vPtDJ4nfhSjZG1wjAZ6K8DOZRXPGsy7R8rgNJAGha-YAnCNCD4XXYQsvUS',
//     'dG5C_v3L-NY:APA91bEbOo2QfWWNgpxZEEdBER4gHyNJ-fEBjOMyw-3hL0olJRG8G3vOh-t92pQQ5Jou-L-foY6dbBMpNGcbzYNBe1_ttzVicSYl79UAlY7aX0JmlxwulL9ezv-iM-Yy3mNqlmS2ZzwZ',
//     'fOEG6TNO3FM:APA91bFThaOKrMpmdjX5i7fYPFxtv_IbKO3yBVUIFg7wXTno5Mg2VnHczJ30HjVoy64m9qIlOLfJ1N_JUsZ1Du1r_-S5uzjujSOgedoljsE0UGytPz6dL_jBcb45bTSnQkS9qdC9KqXZ'
//   ];
//   // var client = http.Client();

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         body: Center(
//       child: Container(
//         child: FlatButton(
//             onPressed: () async {
//               // print("hi");
//               try {
//                 await http.post(
//                   'https://fcm.googleapis.com/fcm/send',
//                   headers: <String, String>{
//                     'Content-Type': 'application/json',
//                     'Authorization': 'key=$serverToken',
//                   },
//                   body: jsonEncode(
//                     <String, dynamic>{
//                       'notification': <String, dynamic>{
//                         'body': 'this is a body',
//                         'title': 'this is a title4'
//                       },
//                       // 'priority': 'high',
//                       'data': <String, dynamic>{
//                         'click_action': 'FLUTTER_NOTIFICATION_CLICK',
//                         'id': '1',
//                         'status': 'done',
//                         'message': "can you teach chemsitry for 2nd standard?"
//                       },
//                       'registration_ids': [
//                         "dG5C_v3L-NY:APA91bEbOo2QfWWNgpxZEEdBER4gHyNJ-fEBjOMyw-3hL0olJRG8G3vOh-t92pQQ5Jou-L-foY6dbBMpNGcbzYNBe1_ttzVicSYl79UAlY7aX0JmlxwulL9ezv-iM-Yy3mNqlmS2ZzwZ",
//                         "dfKEjW7Cr4M:APA91bEJlw9P_VMwu4DjQOvos35gQJDEPyWKiuEC2Fy-Mx-ANBwQNVhrJwvBXKhns6hrbVyj9otaw6UAKD0SvjUwdOE5nBO77EEuJaHuEFRhkTmQbqTjLpw2_5_qLufcrJOnKJxj0bmQ"
//                       ]
//                     },
//                   ),
//                 );
//               } catch (e) {
//                 print(e.toString());
//               }
//               // finally {
//               //   client.close();
//               // }
//             },
//             child: Text("test")),
//       ),
//     ));
//   }
// }
