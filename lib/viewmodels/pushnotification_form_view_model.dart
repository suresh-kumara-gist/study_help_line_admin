// import 'package:compound/constants/route_names.dart';
import 'dart:convert';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:study_help_line_admin/locator.dart';
import 'package:study_help_line_admin/models/register_tutor.dart';
// import 'package:compound/services/cloud_storage_service.dart';
// import 'package:compound/services/authentication_service.dart';
import 'package:study_help_line_admin/services/dialog_service.dart';
import 'package:study_help_line_admin/services/firestore_service.dart';
import 'package:study_help_line_admin/services/navigation_service.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

import 'base_model.dart';

class PushNotificationFormViewModel extends BaseModel {
  final FirestoreService _firestoreService = locator<FirestoreService>();
  // final CloudStorageService _cloudStorageService =
  //     locator<CloudStorageService>();

  // final AuthenticationService _authenticationService =
  //     locator<AuthenticationService>();
  final DialogService _dialogService = locator<DialogService>();
  final NavigationService _navigationService = locator<NavigationService>();
  final FirebaseMessaging _fcm = FirebaseMessaging();
  final String serverToken =
      'AAAA2yhmBG0:APA91bGg_4nqB7DhqZEOwAls1z8qw4c10YYFPqHLZxegrMv5Y3kOcZJTVfqnv6Pfpsy6gu142P0ZAEC8F-ESXe5FDMGWSvjwcS_wu2Xlv19BPWBSnPdYHLTB0vfFDBaWj0yniIeh9cFO';

  Future sendNotification(
      {@required String selectedClass, @required List selectedSubjects}) async {
    setBusy(true);
    // String fcmToken = await _fcm.getToken();
    var tokens = await _firestoreService.getRegisteredTutors(
        selectedClass, selectedSubjects);
    if (tokens.isNotEmpty) {
      try {
        // if (tokens.length > 1000) {
        //   tokens.getRange(start, end);
        // }
        int start = 0;
        int end = 1000;

        if (tokens.length > 1000) {
          while (end <= tokens.length) {
            var chunkOfToken = tokens.getRange(start, end);
            await http.post(
              'https://fcm.googleapis.com/fcm/send',
              headers: <String, String>{
                'Content-Type': 'application/json',
                'Authorization': 'key=$serverToken',
              },
              body: jsonEncode(
                <String, dynamic>{
                  'notification': <String, dynamic>{
                    'body': 'this is a body',
                    'title': 'this is a title4'
                  },
                  // 'priority': 'high',
                  'data': <String, dynamic>{
                    'click_action': 'FLUTTER_NOTIFICATION_CLICK',
                    'id': '1',
                    'status': 'done',
                    'message': "can you teach chemsitry for 2nd standard?"
                  },
                  'registration_ids': chunkOfToken
                },
              ),
            );
            start = end;
            if ((end + 10) > tokens.length) {
              end = end + (tokens.length % 10);
            } else {
              end = end + 10;
            }
          }
        } else {
          await http.post(
            'https://fcm.googleapis.com/fcm/send',
            headers: <String, String>{
              'Content-Type': 'application/json',
              'Authorization': 'key=$serverToken',
            },
            body: jsonEncode(
              <String, dynamic>{
                'notification': <String, dynamic>{
                  'body':
                      'Would you like to teach for class ${selectedClass.toUpperCase()} ${selectedSubjects.toString()} ? Reply your Name and Location through WhatsApp. Thank you',
                  'title': 'Tap to reply'
                },
                // 'priority': 'high',
                'data': <String, dynamic>{
                  'click_action': 'FLUTTER_NOTIFICATION_CLICK',
                  'id': '1',
                  'status': 'done',
                  'message':
                      "Would you like to teach for class ${selectedClass.toUpperCase()} ${selectedSubjects.toString()} ? Reply your Name and Location through WhatsApp. Thank you"
                },
                'registration_ids': tokens
              },
            ),
          );
        }
      } catch (e) {
        print(e.toString());
      }
      // finally {
      //   client.close();
      // }

      setBusy(false);
      await _dialogService.showDialog(title: "Status", description: "Success");
    } else {
      setBusy(false);
      await _dialogService.showDialog(
        title: "Status",
        description: "Tutors not found",
      );
    }
    _navigationService.navigateTo("PushNotificationFormView");
  }
}
